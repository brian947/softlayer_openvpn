#!/usr/bin/env python
#
# Brian McSheehy <bmcshee@us.ibm.com>
# (c) IBM Corporation 2017
#
# Ansible OpenVPN configurator
#
 
import sys, socket, struct, os, jinja2

def cidr_to_netmask(cidr):
    network, net_bits = cidr.split('/')
    host_bits = 32 - int(net_bits)
    netmask = socket.inet_ntoa(struct.pack('!I', (1 << 32) - (1 << host_bits)))
    return network, netmask

def get_cidr_netmask(netmask):
    return sum([bin(int(x)).count("1") for x in netmask.split(".")])

def banner():
    print "*****************************************************"
    print "****** Bluemix OpenVPN Configuration Generator ******"
    print "*****************************************************\n\n"

def build_config_file(config):
    f = open(config_file,"w")
    f.write(config)
    f.close

def build_ansible_hosts(ip):
    data = ip + " ansible_user=root"
    f = open(ansible_hosts,"w")
    f.write(data)
    f.close

if __name__ == "__main__":

    banner()

    ansible_hosts = 'hosts'
    config_file = 'defaults/main.yml';
    default_opts = {"openvpn_network" : "10.8.0.0/24"}

    vpn_server_ip_address = raw_input("Enter Bluemix OpenVPN Server IP Address []: ")
    
    openvpn_network = raw_input("Enter OpenVPN Network [" + default_opts["openvpn_network"] + "]: ")
    if not openvpn_network:
        openvpn_network = default_opts["openvpn_network"]

    bluemix_vpn_subnet = raw_input("Enter Bluemix Private Network [0.0.0.0/24]: ")
    if not bluemix_vpn_subnet:
        print "Error: Bluemix Private Network Required. See control.softlayer.com for private VLAN information"
        sys.exit()

    openvpn_network_netmask = cidr_to_netmask(openvpn_network)
    bluemix_vpn_subnet_netmask = cidr_to_netmask(bluemix_vpn_subnet)

    openvpn_server = openvpn_network_netmask[0] + ' ' + openvpn_network_netmask[1]
    openvpn_push_route_subnet = bluemix_vpn_subnet_netmask[0] + ' ' + bluemix_vpn_subnet_netmask[1]

    templateLoader = jinja2.FileSystemLoader( searchpath="./" )
    templateEnv = jinja2.Environment( loader=templateLoader )
    TEMPLATE_FILE = "defaults/ansible_template.j2"
    template = templateEnv.get_template( TEMPLATE_FILE )

    templateVars = { "openvpn_server" : openvpn_server,
                     "bluemix_vpn_subnet" : bluemix_vpn_subnet,
                     "client_vpn_subnet" : openvpn_network,
                     "openvpn_push_route_subnet" : openvpn_push_route_subnet,
                     "openvpn_etcdir" : '{{openvpn_etcdir}}',
                     "inventory_hostname" : '{{inventory_hostname}}'
                   } 
    outputText = template.render( templateVars )

    build_config_file(outputText)
    build_ansible_hosts(vpn_server_ip_address)
    print "Configuration file generated at " + config_file + "\n\n"
    print "Next step -> run: ansible-playbook -i hosts softlayer.yml\n"
